# frozen_string_literal: true

require_relative 'git_handler'
require 'semantic'

# manages changes applied to semantic versions
class SemverManager
  def initialize(options)
    @log = Logger.new(STDOUT)
    @git_handler = GitHandler.new(options)
    @version_prefix = options[:version_prefix].nil? ? '' : options[:version_prefix]
    # clog file relative to git root
    @clog_file = options[:clog_file].nil? ? '' : options[:clog_file]
    # used for writing to clog file
    @clog_full_path = options[:clog_full_path]
    @log.info("version prefix: #{@version_prefix}, clog: #{@clog_file}, clog_full_path: #{@clog_full_path}")
  end

  def extract_last_version
    last_version = @git_handler.extract_last_version_from_tag

    if last_version.empty?
      @log.error('could not extract last version')
      exit(-1)
    end

    semver = Semantic::Version.new last_version

    if semver.nil?
      @log.error('invalid semantic version')
      exit(-1)
    end

    semver
  end

  def prepend(filename, to_prepend)
    content = to_prepend
    File.open(filename, 'r') { |fil| content += fil.read }
    File.open(filename, 'w') { |fil| fil.write(content) }
  end

  def add_to_changelog(old_semver, new_semver)
    return if @clog_full_path.nil? || @clog_full_path.empty?
    return unless File.exist?(@clog_full_path)

    clog_entry = @git_handler.changelog_entry(old_semver.to_s, new_semver.to_s)

    return false if clog_entry.nil? || clog_entry.empty?

    prepend(@clog_full_path, clog_entry)
    true
  end

  def add_prefix(ver)
    "#{@version_prefix}#{ver}"
  end

  def autobump(test_only = false)
    last_version = extract_last_version
    cmd = @git_handler.extract_command_from_log_message
    entries_added = false
    new_semver = case cmd
                 when GitCmdCode::SKIP
                   @log.info('Skip nothing to do')
                   nil
                 when GitCmdCode::BUMP_MAJOR
                   sv = last_version.increment!(:major)
                   entries_added = add_to_changelog(add_prefix(last_version.to_s), add_prefix(sv.to_s))
                   sv
                 when GitCmdCode::BUMP_MINOR
                   sv = last_version.increment!(:minor)
                   entries_added = add_to_changelog(add_prefix(last_version.to_s), add_prefix(sv.to_s))
                   sv
                 else
                   last_version.increment!(:patch)
                 end

    @git_handler.attach_new_version_tag_to_head(new_semver.to_s, test_only) unless new_semver.nil?

    return if test_only || @clog_file.empty? || !entries_added

    @git_handler.commit_and_push(@clog_file, test_only)
  end
end
