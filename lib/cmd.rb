# frozen_string_literal: true

# GitCmd captures common functionality for git commands
module GitCmd
  def self.wrap(str)
    "\\[#{str}\\]"
  end
end
