#!/bin/bash

# ensure that:
# 1: there are no code issues
# 2: all test cases pass
# 3: code coverage is above 90%

git config --global user.email "you@example.com"
git config --global user.name "Your Name"

gem install bundler:2.1.4
gem install rubocop
gem install simplecov
bundle install
rubocop && bundle exec rake test
exit $?
