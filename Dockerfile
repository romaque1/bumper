# Copyright (c) 2019, Julian Thome
#
# Released under the MIT license
# https://opensource.org/licenses/MIT

FROM ruby:2.6-alpine3.10
MAINTAINER julian.thome.de@gmail.com
RUN apk add bash libffi-dev build-base libxml2-dev git openssh-client
RUN mkdir /opt/bumper
COPY . /opt/bumper
RUN gem install bundler:2.1.2
RUN cd /opt/bumper && bundle install
RUN mkdir /opt/wdir
WORKDIR /opt/wdir
VOLUME ["/opt/wdir"]
CMD ["/bin/bash"]
ENV PATH /opt/bumper:$PATH
