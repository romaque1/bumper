#!/bin/sh

apk update && apk upgrade && apk add --no-cache git

VTAG="$(git describe --tags $(git rev-list --tags --max-count=1))"
ITAG="$IMAGE_TAG-$VTAG"

docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
docker build -t "$ITAG" .
docker push "$ITAG"

exit 0
